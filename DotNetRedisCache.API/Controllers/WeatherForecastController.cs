using DotNetRedisCache.Infrastructure.Redis;
using Microsoft.AspNetCore.Mvc;

namespace DotNetRedisCache.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController(IRedisCacheHelper _redisCacheHelper) : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };


        [HttpGet(Name = "GetWeatherForecast")]
        public async Task<IList<WeatherForecast>> Get()
        {
            var cacheKey = "WeatherForecast";

            var cachedData = await _redisCacheHelper.GetDataAsync<IList<WeatherForecast>>(cacheKey);

            if (cachedData is not null)
            {
                return cachedData;
            }

            cachedData = Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToList();

            await _redisCacheHelper.SetDataAsync<IList<WeatherForecast>>(new(cacheKey, cachedData, 4, 2));

            return cachedData;
        }
    }
}
