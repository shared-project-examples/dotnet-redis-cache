﻿using DotNetRedisCache.Infrastructure.Models;

namespace DotNetRedisCache.Infrastructure.Redis
{
    public interface IRedisCacheHelper
    {
        Task SetDataAsync<T>(RedisCacheModel<T> model);
        Task<T> GetDataAsync<T>(string key);
        Task RemoveDataAsync(string key);
    }
}
