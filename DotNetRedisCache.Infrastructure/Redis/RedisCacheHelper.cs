﻿using Microsoft.Extensions.Caching.Distributed;
using DotNetRedisCache.Infrastructure.Models;
using Newtonsoft.Json;

namespace DotNetRedisCache.Infrastructure.Redis
{
    internal sealed class RedisCacheHelper(IDistributedCache _distributedCache) : IRedisCacheHelper
    {
        public async Task SetDataAsync<T>(RedisCacheModel<T> model)
        {
            var options = new DistributedCacheEntryOptions
            {
                AbsoluteExpiration = DateTimeOffset.UtcNow.AddHours(model.AbsoluteExpiration)
            };

            if (model.SlidingExpiration.HasValue)
            {
                options.SlidingExpiration = TimeSpan.FromHours(model.SlidingExpiration.Value);
            }

            await _distributedCache.SetStringAsync(model.Key, JsonConvert.SerializeObject(model.Data), options);
        }

        public async Task<T?> GetDataAsync<T>(string key)
        {
            var cachedData = await _distributedCache.GetStringAsync(key);

            if (!string.IsNullOrWhiteSpace(cachedData))
            {
                return JsonConvert.DeserializeObject<T>(cachedData);
            }

            return default;
        }

        public async Task RemoveDataAsync(string key) => await _distributedCache.RemoveAsync(key);
    }
}
