﻿using Microsoft.Extensions.DependencyInjection;
using DotNetRedisCache.Infrastructure.Redis;
using Microsoft.Extensions.Configuration;

namespace DotNetRedisCache.Infrastructure
{
    public static class ServiceRegistration
    {
        public static void AddInfrastuctureServices(this IServiceCollection services, IConfiguration configuration)
        {
            AddRedisCache(services, configuration);
        }

        private static void AddRedisCache(IServiceCollection services, IConfiguration configuration)
        {
            services.AddStackExchangeRedisCache(opt =>
            {
                opt.InstanceName = configuration.GetSection("Redis:InstanceName")?.Value;
                opt.Configuration = configuration.GetSection("Redis:Ip")?.Value;
            });

            services.AddScoped<IRedisCacheHelper, RedisCacheHelper>();
        }
    }
}
