﻿
namespace DotNetRedisCache.Infrastructure.Models
{
    public sealed record RedisCacheModel<T>(
        string Key,
        T Data,
        sbyte AbsoluteExpiration,
        sbyte? SlidingExpiration
        );
}

